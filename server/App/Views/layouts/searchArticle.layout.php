<?php foreach ($allArticles as $value):  ?>
    <div class="feed-article" id="feed-article">
        <div class="data-author">
            <?= '<h6>'. $value['first_name']. ' ' . $value['last_name']. '</h6><small>' . $value['created_at'] . '</small>' ?>
        </div>
        <div>
            <h3><?=$value['title'] ?></h3>
        </div>
        <div class="feed-description">
            <p><?=$value['description'] ?></p>
        </div>
        <div class="read-more">
            <a href="/article/<?=$value['id_article'] ?> ">Читать далее</a>
        </div>
    </div>
<?php endforeach; ?>
