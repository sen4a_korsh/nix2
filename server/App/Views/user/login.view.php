<?php !isset($_SESSION['user']) ?: header('Location: '. $_ENV['APP_URL'] . '/feed')
?>
<form class="container form-regist" id="form_login" name="form_login" method="POST" action="/login/ValidUser">
    <div class="title-regist">
        <h1>Вход</h1>
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Email address</label>
        <input type="email" maxlength="100" class="form-control" name="email" placeholder="Введите ваш Email">
    </div>
    <div class="mb-3">
        <label for="exampleInputPassword1" class="form-label">Password</label>
        <input type="password" maxlength="45" class="form-control" name="password" placeholder="Введите ваш пароль">
    </div>
    <div class="mb-3 error" id="error">
    </div>
    <div class="submit-regist">
        <button type="submit" class="btn btn-primary">Войти</button>
    </div>
</form>


<script src="../js/valid.js"></script>
