<?php $_SESSION['user'] ?? header('Location: '. $_ENV['APP_URL'] . '/')?>
<div class="container feed">
    <div class="navbar account-info">
            <div class="user-img">
                <a href="">
                    <img src="../img/icon/user.png" width="70px">
                </a>
            </div>
            <div class="user-data">
                <div><h3><?=$_SESSION['user']['first_name']. ' ' .$_SESSION['user']['last_name']?></h3></div>
                <div><?=$_SESSION['user']['email']?></div>
            </div>
        <div>
            <a href="/user/settings" class="settings" ><img src="../img/icon/settings.png" width="30px"></a>
            <a href="/user/exit" class="btn btn-danger">Выйти</a>
        </div>
    </div>
    <div class="feed-article">
        <form action="/article/create" method="post" enctype="multipart/form-data">
            <div class="title-regist">
                <h4>Новая статья</h4>
            </div>
            <div>
                <lavel>Название статьи:</lavel>
                <input type="text" class="form-control" name="title" required >
            </div>
            <div>
                <lavel>Категория:</lavel>
                <select class="form-select search-select" name="id_sort">
                    <option value="0" selected disabled>Выбирете категорию</option>
                    <?php foreach ($allSort as $value): ?>
                        <option value="<?=$value['id_sort'] ?>"><?=$value['name_sort'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div>
                <lavel>Текст:</lavel>
                <textarea class="form-control" name="description" id="" required cols="30" rows="6"></textarea>
            </div>
            <br>
            <div class="submit-new-article">
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
        </form>
    </div>

    <?php foreach ($userArticle as $value):?>
    <div class="feed-article">
        <div >
            <div class="navbar">
                <div>
                    <small>Создано: <?=$value['created_at']?></small>
                    <br>
                    <small>Обновленно: <?=$value['updated_at']?></small>
                </div>
                <div>
                    <a href="/article/edit/<?=$value['id_article']?>">
                        <img src="../img/icon/edit.png" alt="" width="30px">
                    </a>
                    <a href="/article/delete/<?=$value['id_article']?>">
                        <img src="../img/icon/delete.png" alt="" width="31px">
                    </a>
                </div>
            </div>
            <div>
                <h3><?=$value['title']?></h3>
            </div>

        </div>
        <div class="feed-description">
            <p><?=$value['description']?></p>
        </div>
        <div class="read-more">
            <a href="/article/<?=$value['id_article']?>">Читать далее</a>
        </div>
    </div>
    <?php endforeach;?>
</div>
