<?php $_SESSION['user'] ?? header('Location: '. $_ENV['APP_URL'] . '/'); ?>
<form class="container form-regist" id="form_regist" name="form_registr" method="POST" action="/article/update/<?=$article['id_article']?>">
    <div class="title-regist">
        <h1>Редактировать статью</h1>
    </div>
    <div class="mb-3">
        <label class="form-label">Заголовок:</label>
        <input type="text" maxlength="255" class="form-control" name="title" required value="<?=$article['title']?>" placeholder="Введите заголовок">
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Пол:</label>
        <select class="form-select search-select" name="id_sort">
            <option value="0" selected disabled>Выбирете категорию</option>
            <?php foreach ($allSort as $value): ?>
                <option value="<?=$value['id_sort'] ?>"><?=$value['name_sort'] ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="mb-3">
        <lavel>Текст статьи:</lavel>
        <textarea class="form-control" name="description" id="" required cols="30" rows="6"><?=$article['description']?></textarea>
    </div>
    <div class="mb-3 error" id="error">
    </div>
    <div class="submit-regist">
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </div>
</form>
