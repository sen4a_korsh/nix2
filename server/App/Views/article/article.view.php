<?php $_SESSION['user'] ?? header('Location: '. $_ENV['APP_URL'] . '/');?>
<div class="container feed">
    <div class="feed-article">
        <div>
            <h3><?=$article['title']?></h3>
        </div>
        <br>
        <div>
            <p><?=$article['description']?></p>
        </div>
        <br>
        <div>
            <div>
                <b>Автор: </b> <label><?=$article['first_name'] . ' ' . $article['last_name'] ?></label>
            </div>
            <div>
                <b>Дата написания: </b><label><?=$article['created_at']?></label>
            </div>
            <div>
                <b>Обновленно: </b> <label><?=$article['updated_at']?></label>
            </div>
        </div>
    </div>
    <div class="feed-article" id="block-comments">
        <div>
            <div class="comment-title">
                <h4>Комментарии:</h4>
            </div>
            <form action="/comment/create/<?=$article['id_article']?>" id="article-comments" method="post">
                <div>
                    <textarea class="form-control" name="comment" id="textarea-comment" required cols="30" rows="3"></textarea>
                </div>
                <div class="submit-comment">
                    <button type="submit" class="btn btn-primary">Оставить комментарий</button>
                </div>
            </form>
            <div class="comments" id="comments">
                <?php
                if($comments):
                foreach ($comments as $value): ?>
                <hr>
                <div>
                    <div class="navbar comment-user">
                        <div>
                            <b><?=$value['first_name'] . ' ' . $value['last_name']?></b>
                            <label><?=$value['created_at']?></label>
                        </div>
                        <?php if($_SESSION['user']['id_user'] == $value['id_user']):?>
                        <div>
                            <a class="delete-comment" id="delete-comment" href="/comment/delete/<?=$value['id_comment']?>">
                                <img src="../img/icon/delete.png" alt="" width="31px">
                            </a>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="comment-text">
                        <p><?=$value['comment'] ?></p>
                    </div>
                </div>
                <?php endforeach;
                endif;?>

            </div>
            <div id="test">
            </div>
        </div>
    </div>
</div>
<script src="../js/article-comments.js"></script>
