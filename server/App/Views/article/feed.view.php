<?php $_SESSION['user'] ?? header('Location: '. $_ENV['APP_URL'] . '/')?>

<div class="container feed" id="feed-article">
    <div class="search-feed feed-article">
        <form action="/article/search" class="search-form" id="search-form" method="post">
<!--            <input type="text" class="form-control search-input" placeholder="Искать...">-->
            <select class="form-select search-select" name="id_sort">
                <option value="0" selected disabled>Выбирете категорию</option>
                <?php foreach ($allSort as $value): ?>
                    <option value="<?=$value['id_sort'] ?>"><?=$value['name_sort'] ?></option>
                <?php endforeach; ?>
            </select>

            <button type="submit" class="btn btn-primary search-button">Искать</button>
        </form>
    </div>

    <div id="foundArticles" class="">
        <?php foreach ($allArticles as $value):  ?>
        <div class="feed-article" id="feed-article">

            <div class="data-author">
                <?= '<h6>'. $value['first_name']. ' ' . $value['last_name']. '</h6><small>' . $value['created_at'] . '</small>' ?>
            </div>

            <div>
                <h3><?=$value['title'] ?></h3>
            </div>

            <div class="feed-description">
                <p><?=$value['description'] ?></p>
            </div>
            <div class="read-more">
                <a href="/article/<?=$value['id_article'] ?> ">Читать далее</a>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>
<script src="../js/feed.js"></script>
