<?php


namespace App\Controllers;

use App\Models\CommentModel;
use SamIndustry\Framework\Models\Model;
use SamIndustry\Framework\Controllers\Controller;
use SamIndustry\Framework\Views\View;

/**
 * Class CommentController
 * @package App\Controllers
 */
class CommentController extends Controller
{
    /**
     * @param int $id_article
     */
    public function commentCreate(int $id_article):void
    {
        $id_user = $_SESSION['user']['id_user'];
        CommentModel::commentCreate($_POST, ['id_article'=>$id_article, 'id_user' =>$id_user]);
        $comments = CommentModel::getComments($id_article);
        $this->layout('/layouts/comments', ['comments' => $comments]);
    }

    /**
     * @param int $id_comment
     */
    public function commentDelete(int $id_comment):void
    {
        $id_article = $_POST['id_articles'];
        CommentModel::commentDelete($id_comment);
        $comments = CommentModel::getComments($id_article);
        $this->layout('/layouts/comments', ['comments' => $comments]);
    }

}
