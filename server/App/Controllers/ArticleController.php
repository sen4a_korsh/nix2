<?php

namespace App\Controllers;

use App\Models\ArticleModel;
use App\Models\CommentModel;
use SamIndustry\Framework\Controllers\Controller;
use SamIndustry\Framework\Models\Model;

/**
 * Class ArticleController
 * @package App\Controllers
 */
class ArticleController extends Controller
{

    public function feed():void
    {
        $title = 'Feed';
        $nav = 'main';
        $allArticles = ArticleModel::getAllArticles();
        $allSort = ArticleModel::getAllSort();
        $this->view('article/feed', ['title' => $title, 'nav' => $nav, 'allArticles' => $allArticles, 'allSort' => $allSort]);
    }

    public function searchArticles():void
    {
        $title = 'Article';
        $nav = 'main';
        $allArticles = ArticleModel::searchArticles($_POST);
        $this->layout('/layouts/searchArticle', ['title' => $title, 'nav' => $nav, 'allArticles' => $allArticles]);
    }

    /**
     * @param string $error
     *
     */
    public function getAccount(string $error = ''):void
    {
        $title = 'Account';
        $nav = 'main';
        $userArticle = ArticleModel::getUserArticle();
        $allSort = ArticleModel::getAllSort();
        $this->view('user/account', ['title' => $title, 'nav' => $nav, 'error' => $error, 'userArticle' => $userArticle, 'allSort' => $allSort]);
    }

    /**
     * @param int $id_article
     */
    public function  getArticle(int $id_article):void
    {
         $title = 'Article';
         $nav = 'main';
         $article = ArticleModel::getOneArticle($id_article);
         $comments = CommentModel::getComments($id_article);
         $this->view('article/article', ['title' => $title, 'nav' => $nav, 'article' => $article, 'comments' => $comments]);
    }

    public function createArticle():void
    {
        ArticleModel::createArticle($_POST);
        header('Location: '. $_ENV['APP_URL'] . '/account');
    }

    /**
     * @param int $id
     */
    public function editArticle(int $id):void
    {
        $title = 'Edit Article';
        $nav = 'main';
        $article = ArticleModel::getOneArticle($id);
        $allSort = ArticleModel::getAllSort();
        $this->view('article/edit', ['title' => $title, 'nav' => $nav, 'article' => $article, 'allSort' => $allSort]);
    }

    /**
     * @param int $id
     */
    public function updateArticle(int $id):void
    {
        ArticleModel::updateArticle($id, $_POST);
        header('Location: '. $_ENV['APP_URL'] . "/account");
    }

    /**
     * @param int $id
     */
    public function  deleteArticle(int $id):void
    {
        ArticleModel::deleteArticles($id);
        header('Location: '. $_ENV['APP_URL'] . '/account');
    }

}
