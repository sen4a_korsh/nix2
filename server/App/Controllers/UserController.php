<?php

namespace App\Controllers;

use App\Models\UserModel;
use SamIndustry\Framework\Controllers\Controller;

/**
 * Class UserController
 * @package App\Controllers
 */
class UserController extends Controller
{

    public function create():void
    {
        echo UserModel::create($_POST);
    }

    public function getValidUser():void
    {
        echo UserModel::getValidUser($_POST);
    }
    
    public function login():void
    {
        $title = 'Login';
        $nav = 'login';
        $this->view('user/login', ['title' => $title, 'nav' => $nav]);
    }

    /**
     * @param string $error
     */
    public function register (string $error = ''):void
    {
        $title = 'Registration';
        $nav = 'register';
        $this->view('user/register', ['title' => $title, 'nav' => $nav, 'error' => $error]);
    }

    public function userExit():void
    {
        unset($_SESSION['user']);
        header('Location: '. $_ENV['APP_URL'] . '/');
    }

    public function getSettings():void
    {
        $title = 'Settings';
        $nav = 'main';
        $this->view('user/settings', ['title' => $title, 'nav' => $nav]);
    }

    public function getEditUser():void
    {
        $title = 'Settings';
        $nav = 'main';
        $this->view('user/edit', ['title' => $title, 'nav' => $nav]);
    }

    public function updateUser():void
    {
        UserModel::updateUser($_POST);
        header('Location: '. $_ENV['APP_URL'] . '/user/settings');
    }

    public static function deleteUser():void
    {
        UserModel::deleteUser();
        unset($_SESSION['user']);
        header('Location: '. $_ENV['APP_URL'] . '/');
    }
}
