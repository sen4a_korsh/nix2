<?php

namespace App\Models;

use SamIndustry\Framework\Views\View;
use SamIndustry\Framework\Controllers\Controller;
use SamIndustry\Framework\Models\Model;

/**
 * Class UserModel
 * @package App\Models
 */
class UserModel extends Model
{
    protected string $table = 'users';

    protected bool $created_at = true;
    protected bool $updated_at = true;
    private static string $createError = '<h5>Эта почта занята!</h5>';
    private static string $validError = '<h5>Неверный email или пароль</h5>';


    /**
     * @param array $formData
     * @return string
     */
    public static function create(array $formData):string
    {
        $formData['password'] = password_hash($formData['password'], PASSWORD_BCRYPT);
        $userModel = new self;
        $userEmail = $userModel->whereOne('email', '=', $formData['email'], 'created_at DESC', ['email']);

        if(empty($userEmail)){
            $userModel->insert($formData);
            $id = $userModel->whereOne('email', '=', $formData['email'], 'id_user');
            $_SESSION['user'] = [
                'id_user'=>$id['id_user'],
                'first_name'=> $formData['first_name'],
                'last_name' => $formData['last_name'],
                'email' => $formData['email'],
                'gender_id' => $formData['gender'],
            ];
            return '';
        } else{
            return self::$createError;
        }
    }

    /**
     * @param array $formData
     * @return string
     */
    public static function getValidUser(array $formData) : string
    {
        $userModel = new self;
        $user = $userModel->whereOne('email', '=', $formData['email']);
        if(!empty($user)){
            if(password_verify($formData['password'], $user['password'])){
                $_SESSION['user'] = [
                    'id_user'=>$user['id_user'],
                    'first_name'=> $user['first_name'],
                    'last_name' => $user['last_name'],
                    'email' => $user['email'],
                    'gender_id' => $user['gender'],
                ];
                return '';
            }else{
                return self::$validError;
            }
        }else{
            return self::$validError;
        }

    }

    /**
     * @param array $formData
     */
    public static function updateUser(array $formData):void
    {
        $formData['password'] = password_hash($formData['password'], PASSWORD_BCRYPT);
        $userModel = new self;
        $id = $_SESSION['user']['id_user'];
        if(empty($userModel->update($formData,'id_user', $id))){
            $_SESSION['user']['first_name'] = $formData['first_name'];
            $_SESSION['user']['last_name'] = $formData['last_name'];
            $_SESSION['user']['gender_id'] = $formData['gender'];
        }
    }

     public static function deleteUser():void
    {
        $id = $_SESSION['user']['id_user'];
        $userModel = new self;
        $userModel->delete('id_user', $id);
    }

}
