<?php


namespace App\Models;

use SamIndustry\Framework\Views\View;
use SamIndustry\Framework\Controllers\Controller;
use SamIndustry\Framework\Models\Model;

/**
 * Class CommentModel
 * @package App\Models
 */
class CommentModel extends Model
{
    protected string $table = 'comments';

    protected bool $created_at = true;

    /**
     * @param array $formData
     * @param array $additionalColumns
     */
    public static function commentCreate(array $formData, array $additionalColumns = []):void
    {
        $commentModel = new self;
        $commentModel->insert($formData, $additionalColumns);
    }

    /**
     * @param int $id_article
     * @return array
     */
    public static function getComments(int $id_article):array
    {
        $commentModel = new self;
        return $commentModel->joinWhere(
            'comments',
            'users',
            'id_user',
            'id_user',
            'comments.id_article',
            $id_article,
            'comments.id_comment, comments.id_article, comments.created_at, comments.comment, comments.id_user, users.first_name, users.last_name'
        );
    }

    /**
     * @param int $id_comment
     */
    public static function commentDelete(int $id_comment):void
    {
        $commentModel = new self;
        $commentModel->delete('id_comment', $id_comment);
    }
}
