<?php


namespace App\Models;

use SamIndustry\Framework\Views\View;
use SamIndustry\Framework\Controllers\Controller;
use SamIndustry\Framework\Models\Model;

/**
 * Class ArticleModel
 * @package App\Models
 */
class ArticleModel extends Model
{
    protected string $table = 'articles';

    protected bool $created_at = true;
    protected bool $updated_at = true;

    /**
     * @return array
     */
    public static function getAllArticles():array
    {
        $articleModel = new self;
        return $articleModel->join(
            'articles',
            'users',
            'id_user',
            'id_user',
            'created_at DESC',
            'articles.id_article, articles.title, articles.description, articles.created_at, users.first_name, users.last_name'
        );
    }

    /**
     * @param int $id
     * @return array
     */
    public static function getOneArticle(int $id):array
    {
        $articleModel = new self;
        $article = $articleModel->joinWhere(
            'articles',
            'users',
            'id_user',
            'id_user',
            'articles.id_article',
            $id,
            'articles.id_article, articles.title, articles.description, articles.created_at, articles.updated_at, users.first_name, users.last_name'
        );
        return $article[0];
    }

    /**
     * @return array
     */
    public static function getUserArticle():array
    {
        $articleModel = new self;
        $id = $_SESSION['user']['id_user'];
        return $articleModel->whereFromTable('id_user', '=', $id, 'articles');
    }

    /**
     * @return array
     */
    public static function getAllSort():array
    {
        $articleModel = new self;
        return $articleModel->findAll('sort');
    }

    /**
     * @param array $formData
     */
    public static function createArticle(array $formData):void
    {
        $id_user = $_SESSION['user']['id_user'];
        $articleModel = new self;
        $articleModel->insert($formData, ['id_user' => $id_user]);
    }

    /**
     * @param int $id
     * @param array $formData
     */
    public static function updateArticle(int $id, array $formData):void
    {
        $articleModel = new self;
        $articleModel->update($formData, 'id_article', $id);
    }

    /**
     * @param int $id
     */
    public static function deleteArticles(int $id):void
    {
        $articleModel = new self;
        $articleModel->delete('id_article', $id);
    }

    /**
     * @param array $formData
     * @return array
     */
    public static function searchArticles(array $formData):array
    {
        $id_sort = $formData['id_sort'];
        $articleModel = new self;

        return $articleModel->joinWhere(
            'articles',
            'users',
            'id_user',
            'id_user',
            ' id_sort',
            $id_sort,
            'articles.id_article, articles.title, articles.description, articles.created_at, users.first_name, users.last_name'
        );
    }

}
