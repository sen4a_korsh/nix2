<?php

use App\Controllers\ArticleController;
use App\Controllers\CommentController;
use SamIndustry\Framework\Route;
use App\Controllers\UserController;

Route::add('/', [UserController::class, 'login']);
Route::add('/register', [UserController::class, 'register']);
Route::add('/user/create', [UserController::class, 'create']);
Route::add('/login', [UserController::class, 'login']);
Route::add('/login/ValidUser', [UserController::class, 'getValidUser']);
Route::add('/user/exit', [UserController::class, 'userExit']);
Route::add('/user/settings', [UserController::class, 'getSettings']);
Route::add('/user/edit', [UserController::class, 'getEditUser']);
Route::add('/user/update', [UserController::class, 'updateUser']);
Route::add('/user/delete', [UserController::class, 'deleteUser']);

Route::add('/feed', [ArticleController::class, 'feed']);
Route::add('/account', [ArticleController::class, 'getAccount']);
Route::add('/article/create', [ArticleController::class, 'createArticle']);
Route::add('/article/search', [ArticleController::class, 'searchArticles']);
Route::add('/article/([0-9+])', [ArticleController::class, 'getArticle/$1']);
Route::add('/article/edit/([0-9+])', [ArticleController::class, 'editArticle/$1']);
Route::add('/article/update/([0-9+])', [ArticleController::class, 'updateArticle/$1']);
Route::add('/article/delete/([0-9+])', [ArticleController::class, 'deleteArticle/$1']);

Route::add('/comment/create/([0-9+])', [CommentController::class, 'commentCreate/$1']);
Route::add('/comment/delete/([0-9+])', [CommentController::class, 'commentDelete/$1']);
