<?php

namespace SamIndustry\Framework\Models;

use SamIndustry\Framework\DB;

/**
 * Class Model
 * @package SamIndustry\Framework\Models
 */
abstract class Model
{

    protected DB $pdo;
    protected string $table;


    protected bool $created_at = false;
    protected bool $updated_at = false;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $this->pdo = DB::instance();
    }

    /**
     * @param string $table
     * @return array
     */
    public function findAll(string $table):array
    {
        $sql = "SELECT * FROM $table";
        return $this->pdo->query($sql);
    }

    /**
     * @param string $column
     * @param string $operator
     * @param mixed $data
     * @param string $orderBy
     * @param mixed $columnSearch
     * @return array
     */
    public function whereOne(string $column, string $operator, mixed $data, string $orderBy = 'created_at DESC', mixed $columnSearch = '*'):array
    {
        $result = $this->where($column, $operator, $data, $orderBy, $columnSearch);
        if(empty($result)){
            return [];
        }else{
            return $result[0];
        }
    }

    /**
     * @param string $whereColumn
     * @param string $operator
     * @param mixed $data
     * @param string $orderBy
     * @param mixed $columnSearch
     * @return array
     */
    public function where(string $whereColumn, string $operator, mixed $data, string $orderBy = 'created_at DESC', mixed $columnSearch = '*'):array
    {
        if(!is_string($columnSearch)){
            $columnSearch = implode(",", $columnSearch);
        }
        $sql = "SELECT $columnSearch FROM $this->table WHERE $whereColumn $operator '$data' ORDER BY $orderBy";
        $result = $this->pdo->query($sql);

        return $result;
    }

    /**
     * @param string $column
     * @param string $operator
     * @param mixed $data
     * @param string $table
     * @param string $orderBy
     * @param mixed $columnSearch
     * @return array
     */
    public function whereFromTable(string $column, string $operator, mixed $data, string $table, string $orderBy = 'created_at DESC', mixed $columnSearch = '*'):array
    {
        if(!is_string($columnSearch)){
            $columnSearch = implode(",", $columnSearch);
        }
        $sql = "SELECT $columnSearch FROM $table WHERE $column $operator '$data' ORDER BY $orderBy";
        $result = $this->pdo->query($sql);

        return $result;
    }

    /**
     * @param string $table1
     * @param string $table2
     * @param string $columnTable1
     * @param string $columnTable2
     * @param string $orderBy
     * @param mixed $columnSearch
     * @return array
     */
    public function join(string $table1, string $table2, string $columnTable1, string $columnTable2, string $orderBy = 'created_at DESC', mixed $columnSearch = '*'):array
    {

        if(!is_string($columnSearch)){
            $columnSearch = implode(",", $columnSearch);
        }
        $sql = "SELECT $columnSearch FROM $table1 JOIN $table2 ON $table1.$columnTable1 = $table2.$columnTable2 ORDER BY $orderBy";

        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * @param string $table1
     * @param string $table2
     * @param string $columnTable1
     * @param string $columnTable2
     * @param string $whereColumn
     * @param mixed $whereData
     * @param mixed $columnSearch
     * @return array
     */
    public function joinWhere(string $table1, string $table2, string $columnTable1, string $columnTable2, string $whereColumn, mixed $whereData, mixed $columnSearch = '*'):array
    {

        if(!is_string($columnSearch)){
            $columnSearch = implode(",", $columnSearch);
        }
        $sql = "SELECT $columnSearch FROM $table1 JOIN $table2 ON $table1.$columnTable1 = $table2.$columnTable2 WHERE $whereColumn = $whereData  ORDER BY created_at DESC";
        $result = $this->pdo->query($sql);
        return $result;
    }

    /**
     * @param array $array
     * @param array $additionalColumns
     * @return array
     */
    public function insert(array $array, $additionalColumns = []): array
    {
        $array = array_map(function ($val) {
            return htmlspecialchars($val);
        }, $array);

        if ($this->created_at) {
            $arr = [
                'created_at' => date('Y-m-d H:i:s')
            ];
            $array = array_merge($array, $arr);
        }
        if($this->updated_at){
            $arr = [
                'updated_at' => date('Y-m-d H:i:s')
            ];
            $array = array_merge($array, $arr);
        }
        if(!empty($additionalColumns)){
            $array = array_merge($array, $additionalColumns);
        }

        $keysString = implode(',', array_keys($array));
        $valuesString = implode('\',\'', array_values($array));

        $sql = "INSERT INTO $this->table ($keysString) VALUES ('$valuesString')";

        return $this->pdo->query($sql);
    }

    /**
     * @param array $array
     * @param string $column
     * @param mixed $data
     * @return array
     */
    public function update(array $array, string $column, mixed $data):array
    {

        $array = array_map(function ($val) {
            return htmlspecialchars($val);
        }, $array);

        if ($this->created_at && $this->updated_at) {
            $arr = [
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $array = array_merge($array, $arr);
        }

        $set = '';
        $x = 1;
        foreach ($array as $key => $value){
            $set .= "$key = '$value'";
            if($x < count($array)) {
                $set .= ', ';
            }
            $x++;
        }

        $sql = "UPDATE $this->table SET $set WHERE $column = $data;";
        return $this->pdo->query($sql);
    }

    /**
     * @param string $whereColumn
     * @param mixed $data
     * @return array
     */
    public function delete(string $whereColumn, mixed $data):array
    {
        $sql = "DELETE FROM $this->table WHERE $whereColumn = $data";
        return $this->pdo->query($sql);
    }

}