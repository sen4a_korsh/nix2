<?php

namespace SamIndustry\Framework\Controllers;

use SamIndustry\Framework\Views\View;

/**
 * Class Controller
 * @package SamIndustry\Framework\Controllers
 */
abstract class Controller
{
    /**
     * @var array
     */
    public array $route = [];

    /**
     * Controller constructor.
     * @param $route
     */
    public function __construct($route)
    {
        $this->route = $route;
    }

    /**
     * @param string $viewPath
     * @param array $data
     * @return void
     */
    public function view(string $viewPath, array $data = []):void
    {
        $viewObj = new View($this->route, $viewPath);
        $viewObj->render($data);
    }

    /**
     * @param string $viewPath
     * @param array $data
     */
    public function layout(string $viewPath, array $data = []):void
    {
        $viewObj = new View($this->route, $viewPath);
        $viewObj->renderLayouts($data);
    }
}