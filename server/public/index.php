<?php

define('URI', trim($_SERVER['REQUEST_URI'], ''));
define('ROOT', dirname(__DIR__));

require_once '../vendor/autoload.php';
require_once  (ROOT . '/routes/web.php');

use SamIndustry\Framework\Route;
use Symfony\Component\Dotenv\Dotenv;

session_start();

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL);

$dotenv = new Dotenv();
$dotenv->load(ROOT . '/.env');

Route::dispatch(URI);
