$(document).ready(function (){

    $("#form_regist").submit(function (e){
        e.preventDefault();

        const first_name = $.trim($("#first_name").val());
        const last_name = $.trim($("#last_name").val());
        const email = $.trim($('input[name="email"]').val());
        const gender = $.trim($('select[name="gender"]').val());
        const password = $.trim($("#password").val());

        if(email === '' || password === '' || first_name === '' || last_name === ''){
            $('#error').html('<h5>Заполните все поля!</h5>');
        }else{
            $.ajax({
                url: '/user/create',
                type: 'POST',
                dataType: 'text',
                data: {
                    first_name: first_name,
                    last_name: last_name,
                    email: email,
                    gender: gender,
                    password: password
                },
                success (data){
                    if(!data){
                        document.location.href = '/feed';
                    }else{
                        $('#error').html(data);
                    }
                }
            })
        }
    })

    $("#form_login").submit(function (e){
        e.preventDefault();

        const email = $.trim($('input[name="email"]').val());
        const password = $.trim($('input[name="password"]').val());

        if(email === '' || password === ''){
            $('#error').html('<h5>Заполните все поля!</h5>');
        }else{
            $.ajax({
                url: '/login/ValidUser',
                type: 'POST',
                dataType: 'text',
                data: {
                    email: email,
                    password: password
                },
                success (data){
                    if(!data){
                        document.location.href = '/feed';
                    }else{
                        $('#error').html(data);
                    }
                }
            })
        }
    })

})
