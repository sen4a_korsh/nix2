$(document).ready(function (){

    $("#search-form").submit(function (e) {
        e.preventDefault();

        const id_sort = $.trim($('select[name="id_sort"]').val());

        if(id_sort){
            $.ajax({
                url: '/article/search',
                type: 'POST',
                dataType: 'html',
                data: {
                    id_sort: id_sort
                },
                success (data){
                    if(!data){
                        const notFoundMess =
                            '<div class="notFoundArticles" id="notFoundArticles">\n' +
                            '<h2>По данному запрос ничего не найдено</h2>' +
                            '</div>';

                        $('#foundArticles').html(notFoundMess);
                    }else {
                        $('#foundArticles').html(data);
                    }
                }
            })
        }

    })
})
