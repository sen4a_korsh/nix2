$(document).ready(function (){

    $("#article-comments").submit(function (e) {
        e.preventDefault();

        const comment = $.trim($('textarea[name="comment"]').val());

        let arr = document.location.pathname.split('/');
        let id_articles = arr.slice(-1)[0];

        if(comment){
            $.ajax({
                url: '/comment/create/'+id_articles,
                type: 'POST',
                dataType: 'html',
                data: {
                    comment: comment
                },
                success (data){
                    $('#block-comments').html(data);
                }
            })
        }
    })

    $('.delete-comment').click(function (e){
        e.preventDefault();

        let urlArticle = document.location.pathname.split('/');
        let id_articles = urlArticle.slice(-1)[0];

        $.ajax({
            url: $(this).attr('href'),
            type: 'POST',
            dataType: 'html',
            data: {
                id_articles: id_articles,
            },
            success (data){
                $('#block-comments').html(data)
            }
        })
    })
})
